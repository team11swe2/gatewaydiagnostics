import unittest
import d1, d2, d3, d4, d5, d6, d7

class Test(unittest.TestCase):
    # d1
    def test1d1(self):
        self.assertIsNot("Error" not in d1.output, False)

    def test2d1(self):
        self.assertIsNot(d1.output, "")

    # d2
    def test1d2(self):
        self.assertIsNot("Error" not in d2.output, False)

    def test2d2(self):
        self.assertIsNot(d2.output, "")

    def test3d2(self):
        self.assertEqual(len(d2.output), 132)
        # will pass if all show OK

    #d3
    def test1d3(self):
        self.assertIsNot("Error" not in d3.output, False)

    def test2d3(self):
        self.assertIsNot(d3.output, "")

    def test3d3(self):
        self.assertEqual(len(d3.output), 115)

    # d4
    def test1d4(self):
        self.assertIsNot("Error" not in d4.output, False)

    def test2d4(self):
        self.assertIsNot(d4.output, "")

    def test3d4(self):
        self.assertGreaterEqual(len(d4.output), 62)
        self.assertLessEqual(len(d4.output),63)
        # some Windows line ending thing, necessary to pass unit test here

    # d5
    def test1d5(self):
        self.assertIsNot("Error" not in d5.output, False)

    def test2d5(self):
        self.assertIsNot(d5.output, "")

    def test3d5(self):
        self.assertEqual(len(d5.output), 66)

    # d6
    def test1d6(self):
        self.assertIsNot("Error" not in d6.output, False)

    def test2d6(self):
        self.assertIsNot(d6.output, "")

    def test3d6(self):
        self.assertEqual(len(d6.output), 187)

    # d7
    def test1d7(self):
        self.assertIsNot("Error" not in d7.output, False)

    def test2d7(self):
        self.assertIsNot(d7.output, "")

    def test3d7(self):
        self.assertEqual(len(d7.output), 123)

if __name__ == '__main__':
    unittest.main()
