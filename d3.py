# Diagnostic program 3
# CPU Float Math test
# Gateway Diagnostics, Marcus Eicher

from timeit import default_timer as timer
import math
start = timer()
output = ""
a = 12.3456
b = 54.3210
c = 1.0987654321
d = 0.1234567890

testAdd = 0
testSub = 0
testMult = 0
testDiv = 0

try:
    # addition
    tmp = a + b
    tmp = float(format((tmp), '.10f'))
    if (tmp == 66.6666):  testAdd += 1

    tmp = c + d
    tmp = float(format((tmp), '.10f'))
    if (tmp == 1.2222222211):  testAdd += 1

    if testAdd == 2: output = output + "Float math test +: OK\n"
    else: output = output + "Float math test +: Error\n"


    # subtraction
    tmp = a - b
    tmp = float(format((tmp), '.10f'))
    if (tmp == -41.9754):  testSub += 1

    tmp = c - d
    tmp = float(format((tmp), '.10f'))
    if (tmp == 0.9753086431):  testSub += 1

    if testSub == 2: output = output + "Float math test -: OK\n"
    else: output = output + "Float math test -: Error\n"


    # multiplication
    tmp = a * b
    tmp = float(format((tmp), '.10f'))
    if tmp == 670.6253376: testMult += 1

    tmp = c * d
    tmp = float(format((tmp), '.10f'))
    if tmp == 0.1356500521: testMult += 1

    if testMult == 2: output = output + "Float math test *: OK\n"
    else: output = output + "Float math test *: Error\n"


    # division
    tmp = a / b
    tmp = float(format((tmp), '.10f'))
    if tmp == 0.2272712211: testDiv += 1

    tmp = c / d
    tmp = float(format((tmp), '.10f'))
    if tmp == 8.900000081: testDiv += 1

    if testDiv == 2: output = output + "Float math test /: OK\n"
    else: output = output + "Float math test /: Error\n"

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output

    print(output)

except:
    print("Error doing float calculations\n")