# Diagnostic program 7
# CPU Integer Math test
# Gateway Diagnostics, Marcus Eicher

from timeit import default_timer as timer
start = timer()

output = ""
a = 2
b = 3
c = 11
d = 236
e = 1024
f = 512
g = 8

testAdd = 0
testSub = 0
testMult = 0
testDiv = 0

try:
    # addition
    if (a + b == 5):  testAdd += 1
    if (c + d == 247): testAdd += 1
    if (b + c == 14): testAdd += 1

    if testAdd == 3: output = output + "Integer math test +: OK\n"
    else: output = output + "Integer math test +: Error\n"

    # subtraction
    if (b - a == 1): testSub += 1
    if (d - c == 225): testSub += 1
    if (d - b == 233): testSub += 1

    if testSub == 3: output = output + "Integer math test -: OK\n"
    else: output = output + "Integer math test -: Error\n"

    # multiplication
    if (a * b == 6):  testMult += 1
    if (c * d == 2596):  testMult += 1

    if testMult == 2: output = output + "Integer math test *: OK\n"
    else: output = output + "Integer math test *: Error\n"

    # division
    if (e / g == 128): testDiv += 1
    if (f / g == 64): testDiv += 1

    if testDiv == 2: output = output + "Integer math test /: OK\n"
    else: output = output + "Integer math test /: Error\n"

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output

    print(output)

except:
    print("Error doing integer calculations\n")