# Diagnostic program 2
# File check for programs
# Gateway Diagnostics, Marcus Eicher

import os.path
from timeit import default_timer as timer
start = timer()
output = ""
d1ok = 0
d2ok = 0
d3ok = 0
d4ok = 0
d5ok = 0
d6ok = 0
d7ok = 0

try:
    if os.path.exists("d1.py"): d1ok = 1
    if os.path.exists("d2.py"): d2ok = 1
    if os.path.exists("d3.py"): d3ok = 1
    if os.path.exists("d4.py"): d4ok = 1
    if os.path.exists("d5.py"): d5ok = 1
    if os.path.exists("d6.py"): d6ok = 1
    if os.path.exists("d7.py"): d7ok = 1

    if d1ok:    output = output + "Program d1: OK\n"
    else:       output = output + "Program d1: Error\n"

    if d2ok:    output = output + "Program d2: OK\n"
    else:       output = output + "Program d2: Error\n"

    if d3ok:    output = output + "Program d3: OK\n"
    else:       output = output + "Program d3: Error\n"

    if d4ok:    output = output + "Program d4: OK\n"
    else:       output = output + "Program d4: Error\n"

    if d5ok:    output = output + "Program d5: OK\n"
    else:       output = output + "Program d5: Error\n"

    if d6ok:    output = output + "Program d6: OK\n"
    else:       output = output + "Program d6: Error\n"

    if d7ok:    output = output + "Program d7: OK\n"
    else:       output = output + "Program d7: Error\n"

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output

    print(output)

except OSError as e:
    print("Error accessing program files:\n" + e.strerror)
except:
    print("Error doing program file check diagnostic")



