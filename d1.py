# Diagnostic program 1
# Battery status & power source
# Gateway Diagnostics, Marcus Eicher

import psutil
output = ""

try:
    battery = psutil.sensors_battery()

    if battery.power_plugged == False: plugged = "Battery"
    else: plugged = "Power supply (AC)"

    output = "Charge: " + str(battery.percent) + "%, power source: " + plugged + "\n"

    print(output)

except:
    print("Error getting battery info\n")