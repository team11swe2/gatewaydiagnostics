# Diagnostic program 6
# CPU Prime number test
# Gateway Diagnostics, Marcus Eicher

from timeit import default_timer as timer
start = timer()

output = ""

try:
    primesHardcoded = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
    primes = []

    for possiblePrime in range(2, 100):
        isPrime = True

        for num in range(2, possiblePrime):
            if possiblePrime % num == 0:
                isPrime = False

        if isPrime:
            primes.append(possiblePrime)

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output

    output = output + "Prime numbers to 100: " + ', '.join(map(str, primes)) + "\n"
    if primes == primesHardcoded: output = output + "Output matches hardcoded prime number list\n"
    else: output = output + "Error, generated prime number list is wrong\n"

    print(output)

except:
    print("Error calculating prime numbers\n")