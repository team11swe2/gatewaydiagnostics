# Diagnostic program 4
# Memory diagnostic
# Gateway Diagnostics, Marcus Eicher

import random, string
from timeit import default_timer as timer
start = timer()
output = ""

try:
    L1 = []

    print("Creating million item list with random data (10 bytes), this will take a few seconds...")

    for x in range(1000000):
        tmpstr = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(10))

        L1.append(tmpstr)

    if len(L1) == 1000000: output = output + "Create large list with entries: OK\n"
    else: output = output + "Create large list with entries: Error, entry count not correct\n"

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output

    print(output)

except:
    print("Error while creating large list\n")