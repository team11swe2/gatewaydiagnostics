# Diagnostic program 5
# Storage diagnostic
# Gateway Diagnostics, Marcus Eicher

import random, string, time, os
from timeit import default_timer as timer
start = timer()
output = ""

try:
    # make 1MB of random data
    tmpstr = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(1048576))

    # write file
    try:
        file = open("testfile.txt", "w")
        file.write(tmpstr)
        file.close()
        output = "File write test: OK\n"
    except:
        output = "File write test: Error, could not write test file\n"

    # reading file
    file = open("testfile.txt", "r")
    fromFile = file.read(1048576)

    # compare written file with generated string
    if fromFile == tmpstr: output = output + "File read test: OK"
    else: output = "File read test: Error, content mismatch"
    file.close()

    # delete testfile
    os.remove("testfile.txt")

    end = timer()
    duration = format((end - start), '.5f')
    output = "Time elapsed (ms): " + str(duration) + "\n" + output +"\n"

    print(output)

except OSError as e:
    print("Error processing test file:\n" + e.strerror)
except:
    print("Error doing storage diagnostic")